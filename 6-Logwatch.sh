#!/bin/bash

echo "Type in the email address where you want to email logwatch logs and press [ENTER]"

read $mailto

sed -i "s|/usr/sbin/logwatch --output mail.*|/usr/sbin/logwatch --output mail --mailto $mailto --detail high|g" /etc/cron.daily/00logwatch
