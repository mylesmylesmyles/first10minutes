#!/bin/bash

echo "Please enter the username of the user who will be logging in followed by [ENTER]"

read usr

echo "Please enter the IP/VPN from which you will be connecting to this server followed by [ENTER]"

read ip

sed -i "s|PermitRootLogin .*|PermitRootLogin no|g" /etc/ssh/sshd_config
sed -i "s|PasswordAuthentication .*|PasswordAuthentication no|g" /etc/ssh/sshd_config
sed -i "s|AllowUsers .*|AllowUsers $usr@$ip|g" /etc/ssh/sshd_config

sudo service ssh restart
