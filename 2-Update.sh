#!/bin/bash

sudo apt update
sudo apt upgrade -y
sudo apt-get install unattended-upgrades ufw fail2ban logwatch -y
sudo apt-get clean all
sudo apt-get autoclean

cat << EOF > /etc/apt/apt.conf.d/10periodic
APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Download-Upgradeable-Packages "1";
APT::Periodic::AutocleanInterval "7";
APT::Periodic::Unattended-Upgrade "1";
EOF

echo "Now some manual editing"
echo "Open /etc/apt/apt.conf.d/50unattended-upgrades and make it look like this:"
cat <<EOF
Unattended-Upgrade::Allowed-Origins {   
    "\${distro_id}:\${distro_codename}";
    "\${distro_id}:\${distro_codename}-security";        
    "\${distro_id}ESM:\${distro_codename}";
    //"\${distro_id}:\${distro_codename}-updates";
};
EOF
