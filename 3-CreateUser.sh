#!/bin/bash

echo "Please enter your username followed by [ENTER]:"

read usr

useradd $usr
mkdir /home/$usr
mkdir /home/$usr/.ssh
chmod 700 /home/$usr/.ssh 
usermod -s /bin/bash $usr
usermod -aG sudo $usr

echo "Paste the contents of id_rsa.pub on your local machine followed by [ENTER]:"

read rsa

echo "$rsa" > /home/$usr/.ssh/authorized_keys
chmod 400 /home/$usr/.ssh/authorized_keys
chown $usr:$usr /home/$usr -R

echo "Please enter the passord for $usr"

passwd $usr

